package jp.alhinc.ohata_masato.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

//-----------------------メインメソッド
class CalculateSales{
	public static void main(String[]args) throws IOException{
		HashMap<String,String> branchMap =new HashMap<String,String>();
		HashMap<String,Long>codeMap=new HashMap<String,Long>();
		if(args.length !=1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		String fileNode=args[0];
		if(!fileIn(fileNode,"branch.lst","支店定義",branchMap, codeMap)){
			return;
		}
		if(!fileGet(fileNode,"支店","売上",branchMap, codeMap)){
			return;
		}
		if(!fileOut(fileNode, "branch.out",branchMap, codeMap)){
			return;
		}
	}
//-------------------------ファイルの読み込みメソッド
	public static boolean fileIn(String fileNode,String numberTotal,String definition,
	HashMap<String,String> definitionMap,HashMap<String,Long>totalMap) throws FileNotFoundException{
		File file = new File(fileNode,numberTotal);
		//支店定義ファイルが存在しなければfalseを返す。
		if(!file.exists()){
			System.out.println(definition+"ファイルが存在しません");
			return false;
		}
		FileReader fr =null;
		fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		try{

			//mapを使用して、支店コードと店舗名を結び付ける
			String line;
			while((line = br.readLine())!= null){
				String[] str= line.split(",");
				//str＝brunch.lstの中身をリスト、スプリットしたもの
				if(str[0].matches("[0-9]{3}") && str.length ==2){
					definitionMap.put(str[0],str[1]);
					totalMap.put(str[0],0L);
				}else{
					System.out.println(definition+"ファイルのフォーマットが不正です");
					return false;
				}
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(br !=null){
				try{
					br.close();
				}catch(IOException e){
					//ファイルの中身がnullでない、かつ例外が発生した場合
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
			if(fr !=null){
				try{
					fr.close();
				}catch(IOException g){
					//ファイルの中身がnullでない、かつ例外が発生した場合
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
//-----------------------------値入力メソッド
	public static boolean fileGet(String fileNode,String branchOffice,String getEarnings,
	HashMap<String,String> definitionMap,HashMap<String,Long>totalMap){
		/* 売り上げの値をgetする用の構文*/
		//fileTask=課題フォルダの変数
		File fileTask = new File(fileNode);
		//listRcd=課題フォルダの中身を配列とし、Rcdファイルのみ抽出した変数
		File[] list =fileTask.listFiles();
		ArrayList<File> listRcd =new ArrayList<File>();
		for(int i=0; i<list.length; i++){
			String filter=list[i].getName();
			if(filter.matches("^\\d{8}(.rcd)$") && (list[i].isFile() )){
				listRcd.add(list[i]);
			}
		}
			//売り上げファイル名が連番でない場合エラーをかえす
		Collections.sort(listRcd);
		for(int i=0; i<listRcd.size()-1; i++){
			String fileNumber=listRcd.get(i).getName().substring(0,8);
			String fileNumber2=listRcd.get(i+1).getName().substring(0,8);
			int numberMath =Integer.parseInt(fileNumber);
			int numberMath2 =Integer.parseInt(fileNumber2);
			if( numberMath2-numberMath !=1){
				//連番になっていない場合エラーを返す
				System.out.println(getEarnings+"ファイル名が連番になっていません");
				return false;
			}
		}
		BufferedReader brGet = null;
		for(int i=0; i<listRcd.size();  i++){
		//brGetにて読み込んだ数字8桁.rcdのファイル名をearningsに格納する
			try{
				ArrayList<String> earnings =new ArrayList<String>();
				brGet= new BufferedReader(new FileReader(listRcd.get(i)));
				String lineGet;
				while((lineGet=brGet.readLine()) !=null){
					earnings.add(lineGet);
				}
				//売り上げファイルの行数が1行以下3行以上あればエラーを返す
				if(earnings.size()!=2) {
					System.out.println(listRcd.get(i).getName()+"のフォーマットが不正です");
					return false;
				}
				if(!earnings.get(1).matches("[0-9]+")){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
				//ファイル内、支店定義が不正の場合の処理
				if(!definitionMap.containsKey(earnings.get(0))){
					System.out.println(listRcd.get(i).getName()+"の"+branchOffice+"コードが不正です");
					return false;
				}
				//earnigs=売り上げファイルの変数名　earningsF=売り上げの変数名
				long earningsF =(totalMap.get(earnings.get(0)));
				earningsF+=Long.parseLong(earnings.get(1));
				//合計金額が10桁を超えたら処理を終了する。
				if(earningsF>=10000000000L){
					System.out.println("合計金額が10桁を超えました");
					return false;
				}
				totalMap.put(earnings.get(0),earningsF);
			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}finally{
				try{
					if(brGet !=null){
						brGet.close();
					}
				}catch(IOException e){
					//ファイルの中身がnullでない、かつ例外が発生した場合
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
//-----------------------------出力メソッド
	public static boolean fileOut(String fileNode,String outNumber,
	HashMap<String,String> definitionMap,HashMap<String,Long>totalMap){
		BufferedWriter bw=null;
		FileWriter fileWr=null;
		try{
			File fileOut = new File(fileNode,outNumber);
			fileWr=new FileWriter(fileOut);
			bw=new BufferedWriter(fileWr);
			for (Map.Entry<String,String> entry : definitionMap.entrySet()) {
				bw.write(entry.getKey() + "," + entry.getValue()+","+totalMap.get(entry.getKey()));
				bw.newLine();
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			try{
				if(bw !=null){
					bw.close();
				}
				if(fileWr !=null){
					fileWr.close();
				}
			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}
}